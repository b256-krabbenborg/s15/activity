// console.log("Hello Thursday")

// [SECTION] Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log ("Result of Addition Operator: " + sum);

let difference = x - y;
console.log ("Result of Subtraction Operator: " + difference);

let product = x * y;
console.log ("Result of Multiplication Operator: " + product);

let quotient = x / y;
console.log ("Result of Division Operator: " + quotient);

let modulo = x % y;
console.log ("Result of Modulo Operator: " + modulo);

// [SECTION] Assignment Operators


// Basic Assignment Operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable
let assignmentNumber = 8;

// Addition Assignment Operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log("Result of Addition Assignment Operator is " + assignmentNumber); 
// assignmentNumber = 10


// [SECTION] Multiple Operators and Parenthesis

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// The order of operations can be changed by adding parentheses to the logic
// By adding parentheses "()", the order of operations are changed prioritizing operations inside the parentheses first then following the MDAS rule for the remaining operations.

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// [SECTION] Increment and Decrement Operators
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to
let z = 1;

// The value of "z" is added by a value of one before returning the value and storing it in the variable "increment"

let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one


increment = z++;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);


increment = z++;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

// The value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one

let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

/*
    - Type coercion is the automatic or implicit conversion of values from one data type to another
    - This happens when operations are performed on different data types that would normally not be possible and yield irregular results
    - Values are automatically converted from one data type to another in order to resolve operations
*/

let num1 = '10';
let num2 = 12;

let coercion = num1 + num2;
console.log(coercion);
console.log(typeof coercion)